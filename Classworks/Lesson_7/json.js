
/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Array

*/

var inputText = document.createElement( "input" );
	inputText.type = "text";
	inputText.placeholder = "type your name";

var inputTextArea = document.createElement( "textarea" );
	inputTextArea.placeholder = "type your question";

var checkBox = document.createElement( "input" );
    checkBox.type = "checkbox";

var labelForCheckBox = document.createElement( "label" );
    labelForCheckBox.innerText = "Send instructions by email?";
    labelForCheckBox.appendChild( checkBox );

var submitButton = document.createElement( "input" );
	submitButton.type = "submit";
	
	submitButton.addEventListener( "click", function ( e ) {
		e.preventDefault();

		if ( inputText.value !== "" && inputTextArea.value !== "" ) {
			var userInfo = {
				name: inputText.value,
				question: inputTextArea.value,
				response: checkBox.checked
			}
		} else { var userInfo = "Empty"; }

		var resultJSON = JSON.stringify( userInfo );
		console.log( resultJSON );

		inputText.value = "";
		inputTextArea.value = "";

	});

var form = document.createElement( "form" );
	form.appendChild( inputText );
	form.appendChild( inputTextArea );
	form.appendChild( labelForCheckBox );
	form.appendChild( submitButton );

var container = document.getElementById( "container" );
	container.appendChild( form );