/*
  Задача:

  1. При помощи fetch получить данные:
     http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2

  2. Полученый ответ преобразовать в json вызвав метод .json с объекта ответа
  3. Выбрать случайного человека и передать в следующий чейн промиса
  4. Сделать дополнительный запрос на получение списка друзей человека
     http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2
     И дальше передать обьект:
      {
        name: userName,
        ...
        friends: friendsData
      }

     Подсказка нужно вызвать дополнительный fecth из текущего чейна.
     Для того что бы передать результат выполнения доп. запроса
     в наш первый промис используйте констуркцию:

      .then(
        response1 => {
          return fetch(...)
            .then(
              response2 => {
                ...
                формируете обьект в котором будут данные человека с
                первого запроса, например его name response1.name
                и друзья которые пришли из доп. запроса
              }
            )
        }
      )

  5. Вывести результат на страничку

  + Бонус. Для конвертации обьекта response в json использовать одну
    функцию.

*/

  var containerDiv = document.getElementById( "container" );

  let firstUrl = "http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2";
  let secondUrl = "http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2";

  fetch ( firstUrl )
    .then ( function ( usersInfo ) {      
      return usersInfo.json();
    })

    .then ( function ( json ) {
      var randomUser = json[ Math.floor(Math.random()* json.length) ];
      var resultObject = {
        name: randomUser.name
      };

      return fetch ( secondUrl )
        .then ( function ( userFriendsInfo ) {
          return userFriendsInfo.json();
        })

        .then ( function ( friends ) {
          resultObject.friends = friends[0].friends;

          let p = document.createElement( "p" );
              p.innerText = "User name: " + resultObject.name;
              containerDiv.appendChild( p );

          let ul = document.createElement( "ul" );
              ul.innerText = "User friends: ";

          resultObject.friends.forEach( function ( item ) {
            let li = document.createElement( "li" );
                li.innerText = item.name;
                ul.appendChild( li );
          })
            containerDiv.appendChild( ul );
        })
      }
    )



