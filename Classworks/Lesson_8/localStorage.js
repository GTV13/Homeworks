/*

  Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.

  Задание 2:
    Написать форму логина, которая после отправки данных записывает их в localStorage.
    Если в localStorage есть записть - На страниче вывести "Привет {username}", если нет - вывести окно
    логина.

    + бонус, сделать кнопку "выйти" которая удаляет запись из localStorage и снова показывает форму логина
    + бонус сделать проверку логина и пароля на конкретную запись. Т.е. залогинит пользователя если
    он введет только admin@example.com и пароль 12345678
*/

var mainBlock = document.getElementById( "mainBlock" );

//   Задание 1

// if ( localStorage.getItem( "backgroundColor" ) !== null ) {
//   document.body.style.backgroundColor = localStorage.getItem( "backgroundColor" );
// }

// var colorButton = document.createElement( "button" );
//     colorButton.innerText = "Change color";

//     colorButton.addEventListener( "click", function () {
//         let randomColor = "rgb(" + Math.floor( Math.random() * 256 ) + "," + Math.floor( Math.random() * 256 ) + "," + Math.floor( Math.random() * 256 ) + ")";
//         localStorage.setItem( "backgroundColor", randomColor );
//         document.body.style.backgroundColor = localStorage.getItem( "backgroundColor" );
//     });

//     mainBlock.appendChild( colorButton );


//   Задание 2

var userInfo = JSON.parse( localStorage.getItem( "loginInfo" ) );

  if ( userInfo !== null ) {
    if ( userInfo.login === "admin@example.com" && userInfo.password === "12345678" ) {
      sessionStart();
    }
    else { 
      loadForm();
    }
  }
  else { loadForm(); } 


function sessionStart() {
  var pattern = /\w+(?=@)/g;
  mainBlock.innerText = "Hello " + userInfo.login.match( pattern );

  var buttonExit = document.createElement( "button" );
      buttonExit.innerText = "Exit";
      buttonExit.addEventListener( "click", function() {
        localStorage.removeItem( "loginInfo" );
        mainBlock.innerHTML = null;
        loadForm();
      });

  mainBlock.appendChild( buttonExit );
}

function loadForm() {
  var inputLogin = document.createElement( "input" );
      inputLogin.type = "text";

  var inputLoginLabel = document.createElement( "label" );
      inputLoginLabel.innerText = "Type your login";
      inputLoginLabel.appendChild( inputLogin );

  var inputPassword = document.createElement( "input" );
      inputPassword.type = "password";

  var inputPasswordLabel = document.createElement( "label" );
      inputPasswordLabel.innerText = "Type your password";
      inputPasswordLabel.appendChild( inputPassword );

  var buttonEnter = document.createElement( "button" );
      buttonEnter.innerText = "Log in";

      buttonEnter.addEventListener( "click", function( e ) {
        e.preventDefault();

        userInfo = {
          login: inputLogin.value,
          password: inputPassword.value
        }

        localStorage.setItem( "loginInfo", JSON.stringify( userInfo ) );

        if ( userInfo.login === "admin@example.com" && userInfo.password === "12345678" ) {
          sessionStart();
        }
        else { 
          alert( "You aren't admin!");
        }

        inputLogin.value = "";
        inputPassword.value = "";
        
      });

  var mainForm = document.createElement( "form" );
      mainForm.appendChild( inputLoginLabel );
      mainForm.appendChild( inputPasswordLabel );
      mainForm.appendChild( buttonEnter );
     
      mainBlock.appendChild( mainForm );
}
