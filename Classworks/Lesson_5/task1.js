/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/


var train = {
	name: "Intercity",
	passangersQuantity: 100,

	stop: function () {
		this.speed = 0;
		document.write( "Поезд с названием " + this.name + " остановился. Скорость " + this.speed + " км/ч. <br>" );
	},

	go: function ( speed ) {
		this.speed = speed;
		document.write( "Поезд с названием " + this.name + " везет " + this.passangersQuantity + " пассажиров, со скоростью " + this.speed + " км/ч. <br>");
	},

	pickupPassangers: function ( newPassangersQuantity ) {
		this.passangersQuantity += newPassangersQuantity;
		document.write( "Теперь пассажиров " + this.passangersQuantity + ". <br>");
	}
}

train.go(250);
train.stop();
train.pickupPassangers(10);
train.go(200);
train.stop();
train.go(150);

