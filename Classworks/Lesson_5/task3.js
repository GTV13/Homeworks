/*
    Задание 3:

    1. Создать ф-ю констурктор которая создаст новую собаку у которой есть имя и порода
    2. Обьект должен иметь пару свойств (Имя, порода)
    3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака ест)
    4. Функция которая перебором выводит все свойства

    // Перебор свойств и методов обьекта
    for (key in obj) {
      console.log( key, obj[key] );
      /* ... делать что-то с obj[key] ...
    // }
*/

function Dog ( name, breed ) {
    this.name = name;
    this.breed = breed;
    this.status = "sleeping";

    this.running = function () {
        this.status = "running";
    };

    this.eating = function () {
        this.status = "eating";
    };

    this.changeStatus = function ( status ) {
        this.status = status;
    }   

    this.showProp = function () {
        console.log("This dog has: ");
        for (key in this) {
            if ( typeof( this[key] ) != "function") {
                console.log( key, ": ", this[key] );
            }
        }
    };
}

var dog1 = new Dog( "someName", "someBreed");
dog1.showProp();
dog1.running();
dog1.showProp();
dog1.eating();
dog1.showProp();
dog1.changeStatus("doing nothing");
dog1.showProp();

