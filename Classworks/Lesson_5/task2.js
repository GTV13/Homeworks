/*
    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент, второй попадает в него через метод .call(obj)
    1.2 Ф-я не принимает никаких аргументов, а необходимые настройки полностью передаются через bind
    1.3 Ф-я принимает фразу для заголовка, обьект с настройками передаем через .apply();
*/

var colors = {
	color: "yellow",
	backgroundColor: "green"
}

// 1.1

function changeColor1 ( colorName ) {
	var h1 = document.createElement( "h1" );
		h1.innerText = "I know how calling works in JS";
		document.body.appendChild( h1 );

	document.body.style.backgroundColor = this.backgroundColor;
	h1.style.color = colorName;
}

changeColor1.call( colors , "blue" );

// 1.2

function changeColor2 () {
	var h1 = document.createElement( "h1" );
		h1.innerText = "I know how binding works in JS";
		document.body.appendChild( h1 );

	document.body.style.backgroundColor = this.backgroundColor;
	h1.style.color = this.color;
}

changeColor2 = changeColor2.bind( colors );
changeColor2();
	
// 1.3

function changeColor3( text ) {
	var h1 = document.createElement( "h1" );
		h1.innerText = text;
		document.body.appendChild( h1 );

	document.body.style.backgroundColor = this.backgroundColor;
	h1.style.color = this.color;
}

changeColor3.apply( colors, [ "I know how applying works in JS" ] );