/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.

    Например:
      encryptCesar('Word', 3);
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1('Sdwq', 3);
      decryptCesar1(...)
      ...
      decryptCesar5(...)

*/


function getLang ( text ) {
  if ( /[а-я]/i.test( text ) ) {
    var firstSymbolCode = 1072;
    var lastSymbolCode = 1103;
  }
  else if ( /[a-z]/i.test( text ) ) {
    var firstSymbolCode = 97;
    var lastSymbolCode = 122;
  }

  return { first: firstSymbolCode,
           last: lastSymbolCode };
}


function encryptCesar ( key, text ) {

  let alphabet =  getLang ( text );

  let currentTextArray = text.toLowerCase().split("");
  let encryptTextArray = [];

  for ( let i = 0; i < currentTextArray.length; i++ ) {
    let currentSymbolCode = currentTextArray[i].charCodeAt();
    let encryptSymbolCode = currentSymbolCode + key;

    if ( encryptSymbolCode > alphabet.last ) {
      encryptSymbolCode = ( encryptSymbolCode - alphabet.last ) + ( alphabet.first - 1 );
    }

    let encryptSymbol = String.fromCharCode( encryptSymbolCode );
    encryptTextArray.push( encryptSymbol );
  }

  console.log( "Исходный текст: ", text, "Результат шифрования:", encryptTextArray.join("") );
}

function decryptCesar ( key, text ) {

  let alphabet =  getLang ( text );

  let encryptTextArray = text.toLowerCase().split("");
  let decryptTextArray = [];

  for ( let i = 0; i < encryptTextArray.length; i++ ) {
    let currentSymbolCode = encryptTextArray[i].charCodeAt();
    let decryptSymbolCode = currentSymbolCode - key;

    if ( decryptSymbolCode < alphabet.first ) {
      decryptSymbolCode = ( alphabet.last + 1 ) - ( alphabet.first - decryptSymbolCode );
    }

    let decryptSymbol = String.fromCharCode( decryptSymbolCode );
    decryptTextArray.push( decryptSymbol );
  }

  console.log( "Исходный текст: ", text, "Результат дешифрования:", decryptTextArray.join("") );
}

encryptCesar ( 20, "виовиа" );
decryptCesar ( 20, "цьвцьф" );


var encryptCesar1 = encryptCesar.bind( null, 1 );
var encryptCesar2 = encryptCesar.bind( null, 2 );
var encryptCesar3 = encryptCesar.bind( null, 3 );
var encryptCesar4 = encryptCesar.bind( null, 4 );
var encryptCesar5 = encryptCesar.bind( null, 5 );

encryptCesar1 ( "abc" );
encryptCesar2 ( "abc" );
encryptCesar3 ( "abc" );
encryptCesar4 ( "abc" );
encryptCesar5 ( "abc" );

var decryptCesar1 = decryptCesar.bind( null, 1 );
var decryptCesar2 = decryptCesar.bind( null, 2 );
var decryptCesar3 = decryptCesar.bind( null, 3 );
var decryptCesar4 = decryptCesar.bind( null, 4 );
var decryptCesar5 = decryptCesar.bind( null, 5 );

decryptCesar1 ( "bcd" );
decryptCesar2 ( "cde" );
decryptCesar3 ( "def" );
decryptCesar4 ( "efg" );
decryptCesar5 ( "fgh" );


