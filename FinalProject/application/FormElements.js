import { myForm, formContent } from "./index";

class Element {
	constructor ( type, name, label ) {
		this.type = type;
		this.name = name;
		this.label = label;

		this.SetLabel = this.SetLabel.bind( this );
		this.CreateElement = this.CreateElement.bind( this );
		this.addToFormContent = this.addToFormContent.bind( this );
	}

	CreateElement () {
		let newElement = document.createElement( "input" );
    		newElement.type = this.type;
    		newElement.name = this.name;

    	return newElement;
	}

	SetLabel ( newElement ) {
    	let labelForNewElement = document.createElement( "label" );
    		labelForNewElement.innerText = this.label;
    		labelForNewElement.appendChild( newElement );
    				
    	return labelForNewElement;
	}

	addToFormContent () {
		let newTag = document.createElement( "input" );
			newTag.type = "checkbox";
			newTag.dataset.parent = this.name;

		let labelForTag = document.createElement( "label" );
			labelForTag.innerText = `${ this.type } #${ newTag.dataset.parent }`;
			labelForTag.appendChild( newTag );

		formContent.appendChild( labelForTag );
	}
}

class InputText extends Element {
	constructor ( type, name, label, value, required, placeholder ) {
        super ( type, name, label );
		this.value = value;
		this.required = required;
		this.placeholder = placeholder;
		this.label = label;
    }

    addToForm ( div ) {
    	let newText = this.CreateElement();
    		if ( this.value !== "" ) {
    			newText.setAttribute( "value", this.value );
    		}
    		
    		if ( this.placeholder !== "" ) {
    			newText.placeholder = this.placeholder;
    		}

    		if ( this.required === true ) {
    			newText.setAttribute( "required", this.required );
    		}

    		if ( this.label !== "" ) {
    			myForm.appendChild( this.SetLabel( newText ) );
    		} else {
    			myForm.appendChild( newText );
    		}
    }
}

class InputDate extends Element {
	constructor ( type, name, label ) {
        super ( type, name, label);
    }

    addToForm () {
    	let newDate = this.CreateElement();

    		if ( this.label !== "" ) {
    			myForm.appendChild( this.SetLabel( newDate ) );
    		} else {
    			myForm.appendChild( newDate );
    		}
    }
}

class InputColor extends Element {
	constructor ( type, name, label ) {
        super ( type, name, label );
	}

	addToForm () {
    	let newColor = this.CreateElement();
    		if ( this.label !== "" ) {
    			myForm.appendChild( this.SetLabel( newColor ) );
    		} else {
    			myForm.appendChild( newColor );
    		}
    }
}

class InputPassword extends Element {
	constructor ( type, name, required, placeholder, minlength, maxlength, label ) {
        super ( type, name, label );
		this.required = required;
		this.placeholder = placeholder;

		this.minlength = minlength;
		this.maxlength = maxlength;
	}

	addToForm () {
    	let newPassword = this.CreateElement();
    		if ( this.placeholder !== "" ) {
    			newPassword.placeholder = this.placeholder;
    		}

    		if ( this.required === true ) {
    			newPassword.setAttribute( "required", this.required );
    		}

    		if ( this.minlength !== "" ) {
    			newPassword.setAttribute( "minlength", this.minlength );
    		}

    		if ( this.maxlength !== "" ) {
    			newPassword.setAttribute( "maxlength", this.maxlength );
    		}

    		if ( this.label !== "" ) {
    			myForm.appendChild( this.SetLabel( newPassword ) );
    		} else {
    			myForm.appendChild( newPassword );
    		}
    }
}

class InputEmail extends Element {
	constructor ( type, name, required, placeholder, label ) {
        super ( type, name, label );
		this.required = required;
		this.placeholder = placeholder;
	}

	addToForm () {
    	let newEmail = this.CreateElement();
    		if ( this.placeholder !== "" ) {
    			newEmail.placeholder = this.placeholder;
    		}

    		if ( this.required === true ) {
    			newEmail.setAttribute( "required", this.required );
    		}

    		if ( this.label !== "" ) {
    			myForm.appendChild( this.SetLabel( newEmail ) );
    		} else {
    			myForm.appendChild( newEmail );
    		}
    }
}

class InputRange extends Element {
	constructor ( type, name, step, min, max, label ) {
        super ( type, name, label );
		this.step = step;
		this.min = min;
		this.max = max;
	}

	addToForm () {
    	let newRange = this.CreateElement();
    		if ( this.step !== "" ) {
    			newRange.setAttribute( "step", this.step );
    		}

    		if ( this.min !== "" ) {
    			newRange.setAttribute( "min", this.min );
    		}

    		if ( this.max !== "" ) {
    			newRange.setAttribute( "max", this.max );
    		}

    		if ( this.label !== "" ) {
    			myForm.appendChild( this.SetLabel( newRange ) );
    		} else {
    			myForm.appendChild( newRange );
    		}
    }
}

class InputSubmit extends Element {
	constructor ( type, name, value ) {
        super ( type, name );
		this.value = value;
	}

	addToForm () {
    	let newSubmit = this.CreateElement();
    		newSubmit.value = this.value;

    		myForm.appendChild( newSubmit );
    }
}

class InputReset extends Element {
	constructor ( type, name, value ) {
        super ( type, name );
		this.value = value;
	}

	addToForm () {
    	let newReset = this.CreateElement();
    		newReset.value = this.value;

    		myForm.appendChild( newReset );
    }
}

class InputRadio extends Element {
	constructor ( type, name, value, checked, label ) {
        super ( type, name, label );
		this.value = value;
		this.checked = checked;
	}

	addToForm () {
    	let newRadio = this.CreateElement();
	    	if ( this.value !== "" ) {
	    		newRadio.value = this.value;
	    	}

    		if ( this.checked === true ) {
    			newRadio.setAttribute( "checked", this.checked );
    		}

    		if ( this.label !== "" ) {
    			myForm.appendChild( this.SetLabel( newRadio ) );
    		} else {
    			myForm.appendChild( newRadio );
    		}
    }
}

class InputCheckbox extends Element {
	constructor ( type, name, value, checked, required, label ) {
        super ( type, name, label );
		this.value = value;
		this.checked = checked;
		this.required = required;
	}

	addToForm () {
    	let newCheckbox = this.CreateElement();
    		if ( this.value !== "" ) {
	    		newCheckbox.value = this.value;
	    	}

    		if ( this.checked === true ) {
    			newCheckbox.setAttribute( "checked", this.checked );
    		}

    		if ( this.required === true ) {
    			newCheckbox.setAttribute( "required", this.required );
    		}

    		if ( this.label !== "" ) {
    			myForm.appendChild( this.SetLabel( newCheckbox ) );
    		} else {
    			myForm.appendChild( newCheckbox );
    		}
    }
}

class TextArea extends Element {
	constructor ( type, name, label, placeholder, rows ) {
       	super( type, name, label );
		this.placeholder = placeholder;
		this.rows = rows;
	}

	addToForm () {
    	let newTextArea = document.createElement( `${ this.type }` );
    		newTextArea.name = this.name;
    		if ( this.placeholder !== "" ) {
	    		newTextArea.placeholder = this.placeholder;
	    	}
    		
    		if ( this.rows !== "" ) {
    			newTextArea.setAttribute( "rows", this.rows );;
    		}

    		if ( this.label !== "" ) {
    			myForm.appendChild( this.SetLabel( newTextArea ) );
    		} else {
    			myForm.appendChild( newTextArea );
    		}
    }
}

export { InputText, InputPassword, InputEmail,
		 InputDate, InputColor, InputRange,
		 InputSubmit, InputReset,
		 InputRadio, InputCheckbox,
		 TextArea };




