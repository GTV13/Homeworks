
import { InputText, InputPassword, InputEmail,
		 InputDate, InputColor, InputRange,
		 InputSubmit, InputReset,
		 InputRadio, InputCheckbox,
		 TextArea } from "./FormElements";

function showTextWindow () { 
	let nameField = createInput( "Text name", "text", "name" );
	let valueField = createInput( "Text value", "text", "value" );
	let placeholderField = createInput( "Text placeholder", "text", "placeholder" );
	let labelField = createInput( "Text label", "text", "label" );

	let required = createCheckboxInput( "Required?", "required" );
	let requiredField = required.elementField;
	let labelForRequiredField = required.labelforElementField;

	let addButton = document.createElement( "button" );
		addButton.innerText = "Add to form";

		addButton.addEventListener( "click", function () {

			if ( nameField.value !== "" ) {
				let newText = new InputText( "text", nameField.value, labelField.value, valueField.value, requiredField.checked, placeholderField.value );
					newText.addToForm( event.target.parentNode );
					newText.addToFormContent();

				nameField.value = "";
				valueField.value = "";
				placeholderField.value = "";
				labelField.value = "";
				requiredField.checked = false;
				nameField.placeholder = "Text name";
			} else {
				nameField.placeholder = "REQUIRED";
			}
		});

	return { nameField, valueField, placeholderField, labelForRequiredField, labelField, addButton };	
}

function showPasswordWindow () {
	let nameField = createInput( "Password name", "text", "name" );
	let placeholderField = createInput( "Password placeholder", "text", "placeholder" );
	let labelField = createInput( "Password label", "text", "label" );

	let required = createCheckboxInput( "Required?", "required" );
	let requiredField = required.elementField;
	let labelForRequiredField = required.labelforElementField;

	let minField = createInput( "Min-length of Password", "number" );
	let maxField = createInput( "Max-length of Password", "number" );

	let addButton = document.createElement( "button" );
		addButton.innerText = "Add to form";

		addButton.addEventListener( "click", function () {
			if ( nameField.value !== "" ) {
				let newPassword = new InputPassword( "password", nameField.value, requiredField.checked, placeholderField.value, minField.value, maxField.value, labelField.value );
					newPassword.addToForm();
					newPassword.addToFormContent();

				nameField.value = "";
				placeholderField.value = "";
				labelForRequiredField.value = "";
				minField.value = "";
				maxField.value = "";
				labelField.value = "";
				requiredField.checked = false;
				nameField.placeholder = "Password name";
			} else if ( nameField.value == "" ) {
				nameField.placeholder = "REQUIRED";
			}
		});

	return { nameField, placeholderField, labelForRequiredField, minField, maxField, labelField, addButton };
}

function showEmailWindow () {
	let nameField = createInput( "Email name", "text", "name" );
	let placeholderField = createInput( "Email placeholder", "text", "placeholder" );
	let labelField = createInput( "Email label", "text", "label" );
		
	let required = createCheckboxInput( "Required?", "required" );
	let requiredField = required.elementField;
	let labelForRequiredField = required.labelforElementField;

	let addButton = document.createElement( "button" );
		addButton.innerText = "Add to form";

		addButton.addEventListener( "click", function () {
			if ( nameField.value !== "" ) {
				let newEmail = new InputEmail( "email", nameField.value, requiredField.checked, placeholderField.value, labelField.value );
					newEmail.addToForm();
					newEmail.addToFormContent();

				nameField.value = "";
				placeholderField.value = "";
				labelForRequiredField.value = "";
				labelField.value = "";
				requiredField.checked = false;
				nameField.placeholder = "Email name";
			} else if ( nameField.value == "" ) {
				nameField.placeholder = "REQUIRED";
			}
		});

	return { nameField, placeholderField, labelForRequiredField, labelField, addButton };
}

function showDateWindow () {
	let nameField = createInput( "Date name", "text", "name" );
	let labelField = createInput( "Date label", "text", "label" );

	let addButton = document.createElement( "button" );
		addButton.innerText = "Add to form";

		addButton.addEventListener( "click", function () {
			if ( nameField.value !== "" ) {
				let newDate = new InputDate( "date", nameField.value, labelField.value );
					newDate.addToForm();
					newDate.addToFormContent();

				nameField.value = "";
				labelField.value = "";
				nameField.placeholder = "Date name";
			} else if ( nameField.value == "" ) {
				nameField.placeholder = "REQUIRED";S
			}
		});

	return { nameField, labelField, addButton };
}

function showColorWindow () {
	let nameField = createInput( "Color name", "text", "name" );
	let labelField = createInput( "Color label", "text", "label" );

	let addButton = document.createElement( "button" );
		addButton.innerText = "Add to form";

		addButton.addEventListener( "click", function () {
			if ( nameField.value !== "" ) {
				let newColor = new InputColor( "color", nameField.value, labelField.value );
					newColor.addToForm();
					newColor.addToFormContent();

				nameField.value = "";
				labelField.value = "";
				nameField.placeholder = "Color name";
			} else if ( nameField.value == "" ) {
				nameField.placeholder = "REQUIRED";S
			}
		});

	return { nameField, labelField, addButton };
}

function showRangeWindow () {
	let nameField = createInput( "Range name", "text", "name" );
	let labelField = createInput( "Range label", "text", "label" );

	let stepField = createInput( "Step of Range", "number", "step" );
	let minField = createInput( "Min-value of Range", "number", "min" );
	let maxField = createInput( "Max-value of Range", "number", "max" );

	let addButton = document.createElement( "button" );
		addButton.innerText = "Add to form";

		addButton.addEventListener( "click", function () {
			if ( nameField.value !== "" ) {
				let newRange = new InputRange( "range", nameField.value, stepField.value, minField.value, maxField.value, labelField.value );
					newRange.addToForm();
					newRange.addToFormContent();

				nameField.value = "";
				labelField.value = "";
				stepField.value = "";
				minField.value = "";
				maxField.value = "";
				nameField.placeholder = "Range name";
			} else if ( nameField.value == "" ) {
				nameField.placeholder = "REQUIRED";
			}
		});

	return { nameField, stepField, minField, maxField, labelField, addButton };
}

function showSubmitWindow () {
	let nameField = createInput( "Submit name", "text", "name" );
	let valueField = createInput( "Submit value", "text", "value" );

	let addButton = document.createElement( "button" );
			addButton.innerText = "Add to form";

			addButton.addEventListener( "click", function () {
				if ( nameField.value !== "" ) {
					let newSubmit = new InputSubmit( "submit", nameField.value, valueField.value );
						newSubmit.addToForm();
						newSubmit.addToFormContent();

					nameField.value = "";
					valueField.value = "";
					nameField.placeholder = "Submit name";
				} else if ( nameField.value == "" ) {
					nameField.placeholder = "REQUIRED";
				}
			});

	return { nameField, valueField, addButton };
}

function showResetWindow () {
	let nameField = createInput( "Reset name", "text", "name" );
	let valueField = createInput( "Reset value", "text", "value" );

	let addButton = document.createElement( "button" );
			addButton.innerText = "Add to form";

			addButton.addEventListener( "click", function () {
				if ( nameField.value !== "" ) {
					let newReset = new InputReset( "reset", nameField.value, valueField.value );
						newReset.addToForm();
						newReset.addToFormContent();

					nameField.value = "";
					valueField.value = "";
					nameField.placeholder = "Reset name";
				} else if ( nameField.value == "" ) {
					nameField.placeholder = "REQUIRED";
				}
			});

	return { nameField, valueField, addButton };
}

function showCheckboxWindow () {
	let nameField = createInput( "Checkbox name", "text", "name" );
	let valueField = createInput( "Checkbox value", "text", "value" );
	let labelField = createInput( "Checkbox label", "text", "label" );

	let required = createCheckboxInput( "Required?", "required" );
	let requiredField = required.elementField;
	let labelForRequiredField = required.labelforElementField;

	let checked = createCheckboxInput( "Checked?", "checked" );
	let checkedField = checked.elementField;
	let labelForCheckedField = checked.labelforElementField;

	let addButton = document.createElement( "button" );
		addButton.innerText = "Add to form";

		addButton.addEventListener( "click", function () {
			if ( nameField.value !== "" ) {
				let newCheckbox = new InputCheckbox( "checkbox", nameField.value, valueField.value, checkedField.checked, requiredField.checked, labelField.value );
					newCheckbox.addToForm();
					newCheckbox.addToFormContent();

				nameField.value = "";
				valueField.value = "";
				labelField.value = "";
				requiredField.checked = false;
				checkedField.checked = false;
				nameField.placeholder = "Checkbox name";
			} else {
				nameField.placeholder = "REQUIRED";
			}
		});

	return { nameField, valueField, labelForCheckedField, labelForRequiredField, labelField, addButton };
}

function showRadioWindow () {
	let nameField = createInput( "Radio name", "text", "name" );
	let valueField = createInput( "Radio value", "text", "value" );
	let labelField = createInput( "Radio label", "text", "label" );

	let checked = createCheckboxInput( "Checked?", "checked" );
	let checkedField = checked.elementField;
	let labelForCheckedField = checked.labelforElementField;

	let addButton = document.createElement( "button" );
		addButton.innerText = "Add to form";

		addButton.addEventListener( "click", function () {
			if ( nameField.value !== "" ) {
				let newRadio = new InputRadio( "radio", nameField.value, valueField.value, checkedField.checked, labelField.value );
					newRadio.addToForm();
					newRadio.addToFormContent();

				nameField.value = "";
				valueField.value = "";
				labelField.value = "";
				checkedField.checked = false;
				nameField.placeholder = "Checkbox name";
			} else {
				nameField.placeholder = "REQUIRED";
			}
		});

	return { nameField, valueField, labelForCheckedField, labelField, addButton };
}

function showTextareaWindow () {
	let nameField = createInput( "Textarea name", "text", "name" );
	let placeholderField = createInput( "Textarea placeholder", "text", "placeholder" );
	let labelField = createInput( "Textarea label", "text", "label" );

	let rowsField = createInput( "Rows of Textarea", "number", "rows" );

	let addButton = document.createElement( "button" );
		addButton.innerText = "Add to form";

		addButton.addEventListener( "click", function () {
			if ( nameField.value !== "" ) {
				let newTextarea = new TextArea( "textarea", nameField.value, labelField.value, placeholderField.value, rowsField.value );
					newTextarea.addToForm();
					newTextarea.addToFormContent();

				nameField.value = "";
				placeholderField.value = "";
				rowsField.value = "";
				labelField.value = "";
				nameField.placeholder = "Textarea name";
			} else {
				nameField.placeholder = "REQUIRED";
			}
		});

	return { nameField, placeholderField, rowsField, labelField, addButton };
}

function createInput ( placeholder, type, edition ) {
	let elementField = document.createElement( "input" );
		elementField.type = type;
		elementField.placeholder = placeholder;
		elementField.dataset.edition = edition;

	return elementField;
}

function createCheckboxInput ( text, edition ) {
	let elementField = document.createElement( "input" );
		elementField.type = "checkbox";

	let labelforElementField = document.createElement( "label" );
		labelforElementField.innerText = text;
		labelforElementField.dataset.edition = edition;
		labelforElementField.appendChild( elementField );

	return { elementField, labelforElementField };
}

export  { showTextWindow, showPasswordWindow, showEmailWindow,
		  showDateWindow, showColorWindow, showRangeWindow,
		  showSubmitWindow, showResetWindow,
		  showCheckboxWindow, showRadioWindow,
		  showTextareaWindow };