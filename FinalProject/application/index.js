
import { showTextWindow, showPasswordWindow, showEmailWindow,
		 showDateWindow, showColorWindow, showRangeWindow,
		 showSubmitWindow, showResetWindow,
		 showCheckboxWindow, showRadioWindow,
		 showTextareaWindow } from "./ElementsWindows";

import { saveFormHTML, deleteFormHTML, deleteElement, editElement } from "./FormSettings";

let formOptionsUl = document.getElementById( "options" );

let formOptions = [ "text", "textarea", "checkbox",
					"radio", "date", "color", "password",
					"email", "range", "submit", "reset"];

	formOptions.forEach( ( item ) => {
		let button = document.createElement( "button" );
			button.innerText = item;

		let li = document.createElement( "li" );
			li.id = item;
			li.appendChild( button );
		formOptionsUl.appendChild( li );
	});

let options = document.querySelectorAll( "#options > li" );
	options.forEach( ( option ) => {
		let div = document.createElement( "div" );
			div.classList.add( "currentCreation" );

		let windowElements = {};

		switch ( option.id ) {

			case "text": windowElements = showTextWindow(); break;
			case "textarea": windowElements = showTextareaWindow(); break;
			case "checkbox": windowElements = showCheckboxWindow(); break;
			case "radio": windowElements = showRadioWindow(); break;
			case "date": windowElements = showDateWindow(); break;
			case "color": windowElements = showColorWindow(); break;
			case "password": windowElements = showPasswordWindow(); break;
			case "email": windowElements = showEmailWindow(); break;
			case "range": windowElements = showRangeWindow(); break;
			case "submit": windowElements = showSubmitWindow(); break;
			case "reset": windowElements = showResetWindow(); break;
		}

		for ( let key in windowElements ) {
			div.appendChild( windowElements[ key ] );
		}

		option.appendChild( div );
	});

	options.forEach( ( option ) => {
		let allDiv = document.querySelectorAll( ".currentCreation" );

		let optionButton = option.querySelector( "button" );
			optionButton.addEventListener( "click", function() {
				allDiv.forEach( ( item ) => {
					item.classList.remove( "show" );
				});
				option.querySelector( ".currentCreation" ).classList.add( "show" );
			});
	});

var myForm = document.getElementById( "myForm" );
var formContent = document.getElementById( "formContent" );

var saveFormButton = document.getElementById( "saveForm" );
	saveFormButton.addEventListener( "click", function() {
		saveFormHTML();
	});

var deleteFormButton = document.getElementById( "deleteForm" );
	deleteFormButton.addEventListener( "click", function() {
		deleteFormHTML();
	});

var deleteElementButton = document.getElementById( "deleteElement" );
	deleteElementButton.addEventListener( "click", function() {
		deleteElement();
	});

var editElementButton = document.getElementById( "editElement" );
	editElementButton.addEventListener( "click", function() {
		editElement();
	});

document.addEventListener( "DOMContentLoaded", function () {
	let formHTML = localStorage.getItem( "formHTML" );
	let formTags = localStorage.getItem( "formTags" );

		if ( formHTML !== null && formTags !== null ) {
			myForm.innerHTML = formHTML;
			formContent.innerHTML = formTags;
		}
});

export { myForm, formContent, options };