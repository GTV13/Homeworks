
import { myForm, formContent, options } from "./index";

function saveFormHTML () {
	let formHTML = myForm.innerHTML;
		localStorage.setItem( "formHTML", formHTML );

	let formTags = formContent.innerHTML;
		localStorage.setItem( "formTags", formTags );
}

function deleteFormHTML () {
	myForm.innerHTML = null;
    localStorage.removeItem( "formHTML" );

    formContent.innerHTML = null;
    localStorage.removeItem( "formTags" );
}

function deleteElement () {
	let tagsCheckbox = formContent.querySelectorAll( "label > input[type=checkbox]" );
		tagsCheckbox.forEach( ( item ) => {
			if ( item.checked ) {
				let elementToDelete = myForm.querySelector( `[name="${ item.dataset.parent }"]` );
					if ( elementToDelete.parentNode !== myForm ) {
						elementToDelete.parentNode.remove();
					} else {
						elementToDelete.remove();
					}
				item.parentNode.remove();
			}
		});
}

function editElement () {
	let allDiv = document.querySelectorAll( ".currentCreation" );
		allDiv.forEach( ( item ) => {
			item.classList.remove( "show" );
		});
				
	let tagsCheckbox = formContent.querySelectorAll( "label > input[type=checkbox]" );
		tagsCheckbox.forEach( ( item ) => {
			if ( item.checked ) {
				let elementToEdit = myForm.querySelector( `[name="${ item.dataset.parent }"]` );
				let windowToOpen;

				if ( elementToEdit.tagName === "INPUT" ) {
					windowToOpen = elementToEdit.getAttribute( "type" );
				} else {
					windowToOpen = "textarea";
				}

				options.forEach( ( item ) => {
					if ( item.id === windowToOpen ) {
						let divCurrent = item.querySelector( ".currentCreation" );
							divCurrent.classList.add( "show" );
							saveItem ( divCurrent, elementToEdit );
					}
				});
			}
			item.checked = false;
		});
}

function saveItem ( div, element ) {
	div.querySelector( "button").classList.add( "hide" );
	let attributes =  Array.from( div.children );

	let editButton = document.createElement( "button" );
		editButton.innerText = "Edit element";
		editButton.addEventListener( "click", function() {
			attributes.forEach( ( item ) => {
				if ( item.dataset.edition === "required" ) {
					element.required = item.lastChild.checked;
					element.setAttribute( "required", item.lastChild.checked );
					item.lastChild.checked = false;
				} 
				else if ( item.dataset.edition === "checked" ) {
					element.checked = item.lastChild.checked;
					element.setAttribute( "checked", item.lastChild.checked );
					item.lastChild.checked = false;
				}
				else if ( item.dataset.edition === "label" ) {
					if ( item.value !== "" ) {
						element.parentNode.childNodes[0].nodeValue = item.value;
					}
				}
				else {
					if ( item.value !== "" ) {
						if ( item.dataset.edition === "name" ) {
							let tagsCheckbox = formContent.querySelectorAll( "label > input[type=checkbox]" );
								tagsCheckbox.forEach( ( tag ) => {
									if ( tag.dataset.parent ===  element.name ) {
										tag.dataset.parent = item.value;
										tag.parentNode.childNodes[0].nodeValue =  `${ element.type } #${ tag.dataset.parent }`;
									}
								});
						}
						let attribute = item.dataset.edition;
							element.setAttribute( attribute, item.value );
					}
				}
				item.value = "";
			});

			div.querySelector( "button").classList.remove( "hide" );
			div.classList.remove( "show" );
			editButton.remove();
		});

	div.appendChild( editButton );
}

export { saveFormHTML, deleteFormHTML, deleteElement, editElement };