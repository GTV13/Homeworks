/*
  Задание 1.
  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick +
  + Бонус, использовать 6-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.
  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore
*/


var contentDiv = document.getElementById("content");
var button = document.getElementById("colorButton");

function getRandomInt(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min)) + min;
}

var div = document.createElement("div");
  	div.style.cssText = "width: max-content; height: max-content; background-color: white; position: absolute; margin: auto; top: 0; left: 0; right: 0; bottom: 0;";

var values = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "E", "D", "F"];

function changeColor() {
	// цвет rgb
	var value = "rgb(" + getRandomInt(0, 256) + "," + getRandomInt(0, 256) + "," + getRandomInt(0, 256) + ")";

	// цвет hex

	// Способ 1

	// var value = "#" + getRandomInt(0, 256).toString(16) + getRandomInt(0, 256).toString(16) + getRandomInt(0, 256).toString(16);

	// Способ 2

	// var value = "#";
	// for (var counter = 1; counter <= 6; counter++) {
	// 	value += values[Math.ceil(Math.random()*15)];
	// }
	
	contentDiv.style.backgroundColor = value;
	div.innerText = value;
}
contentDiv.appendChild(div);
