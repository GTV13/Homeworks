/*
  Задание:
    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватарку);
    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков
    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.
    <div id="CommentsFeed"></div>
*/

function Comment ( userName, commentText, avatarUrl ) {
    this.name = userName;
    this.text = commentText;
    this.likes = 0;
    if ( avatarUrl !== undefined && avatarUrl !== "") {
      this.avatarUrl = avatarUrl;
    }

    Object.setPrototypeOf(this, forComment);
}

var forComment = {
  avatarUrl: "https://cdn1.iconfinder.com/data/icons/mix-color-4/502/Untitled-1-512.png",

  like: function () {
    this.likes += 1;
  }
};

var myComment1 = new Comment( "Alex", "cool", "https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-512.png" );
var myComment2 = new Comment( "Bill", "awesome");
var myComment3 = new Comment( "David", "amazing", "" );
var myComment4 = new Comment( "Frank", "excelent");

var CommentsArray = [ myComment1, myComment2, myComment3, myComment4 ];

function AddComment ( CommentsArray ) {
  CommentsArray.forEach ( function ( comment ) {
        
    var imageAvatar = document.createElement( "img" );
        imageAvatar.src = comment.avatarUrl;
        imageAvatar.classList.add( "imageAvatar" );

    var divUserName = document.createElement( "div" );
        divUserName.innerText = comment.name;

    var divText = document.createElement( "div" );
        divText.innerText = comment.text;
        divText.classList.add( "divText" );

    var divLikes = document.createElement( "div" );
        divLikes.innerText = comment.likes;

    var buttonLike = document.createElement( "button" );
        buttonLike.innerText = "Like";
        buttonLike.classList.add( "buttonLike" );

        buttonLike.addEventListener( "click", function () {
          comment.like();
          divLikes.innerText = comment.likes;
        });

    var divComment = document.createElement( "div" );
        divComment.classList.add( "divComment" );
        divComment.appendChild( imageAvatar );
        divComment.appendChild( divUserName );
        divComment.appendChild( divText );
        divComment.appendChild( divLikes );
        divComment.appendChild( buttonLike );

    CommentsFeed.appendChild( divComment );
  });
}


var addCommentsButton = document.createElement( "button" );
    addCommentsButton.innerText = "Add all comments to page";
    addCommentsButton.addEventListener( "click", function () {
      CommentsFeed.innerHTML = null;
      AddComment ( CommentsArray );
    });

var CommentsFeed = document.getElementById( "CommentsFeed" );
    document.body.insertBefore( addCommentsButton,  CommentsFeed );

    
