  /*
    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором
      2. Повесить кнопку онклик
          button1.onclick = function(event) {}
          + бонус: один обработчик на все три кнопки
      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active
      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок
  */

var tabContainer = document.getElementById("tabContainer");
var tabs = tabContainer.getElementsByTagName("div");

var header = document.getElementById("buttonContainer");
var buttons = header.getElementsByTagName("button");

for (let i = 0; i < buttons.length; i++) {
  buttons[i].onclick = function() {
    hideAllTabs();
    if (this.dataset.tab == tabs[i].dataset.tab) {
      tabs[i].classList.add("active");
    }
  }
}

function hideAllTabs() {
  for (let i = 0; i < tabs.length; i++) {
    tabs[i].classList.remove("active");
  }
}
